﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour
{

    public string key;

    // Use this for initialization
    void Start()
    {
        //Translate();
    }

    public void Translate()
    {
        Text text = GetComponent<Text>();
        if (text)
        {
            text.text = LocalizationManager.instance.GetLocalizedValue(key, text.text);
        }
    }
}