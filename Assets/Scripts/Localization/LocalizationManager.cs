﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class LocalizationManager : MonoBehaviour
{

    public static LocalizationManager instance;

    private Dictionary<string, string> localizedText;
    private bool isReady = false;
    private string missingTextString = "Localized text not found";

    public const string LOCALIZATION_FOLDER_NAME = "Localization";

    private LocalizedText[] texts;

    // Use this for initialization
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    public void LoadLocalizedText(string fileName)
    {
        localizedText = new Dictionary<string, string>();
        string filePath = Path.Combine(Application.streamingAssetsPath, LOCALIZATION_FOLDER_NAME);
        filePath = Path.Combine(filePath, fileName);
		//Debug.Log(filePath);

		string dataAsJson;
		LocalizationData loadedData;

		// For android streaming assets acces use WWW
		if (filePath.Contains("://") || filePath.Contains(":///"))
		{
			WWW www = new WWW(filePath);
			while (!www.isDone) { }
			dataAsJson = www.text;

			//dataAsJson = www.text.Trim();

			loadedData = JsonUtility.FromJson<LocalizationData>(dataAsJson);

            for (int i = 0; i < loadedData.items.Length; i++)
            {
                localizedText.Add(loadedData.items[i].key, loadedData.items[i].value);
            }
		}
		else
		{
			if (File.Exists(filePath))
			{
				dataAsJson = File.ReadAllText(filePath);

				loadedData = JsonUtility.FromJson<LocalizationData>(dataAsJson);

                for (int i = 0; i < loadedData.items.Length; i++)
                {
                    localizedText.Add(loadedData.items[i].key, loadedData.items[i].value);
                }
			}
			else
			{
				Debug.LogError("Cannot find file!");
			}
		}


        //Debug.Log("Data loaded, dictionary contains: " + localizedText.Count + " entries");

        isReady = true;
    }

    public string GetLocalizedValue(string key, string lastString)
    {
        string result = lastString;
        if (localizedText.ContainsKey(key))
        {
            result = localizedText[key];
        }

        return result;

    }

    public bool GetIsReady()
    {
        return isReady;
    }

    private void GetAllTextsObjects()
    {
        texts = FindObjectsOfType<LocalizedText>();
    }

    private void TranslateAllTexts()
    {
        foreach (LocalizedText l in texts)
        {
            l.Translate();
        }
    }

    public void TranslateAll()
    {
        GetAllTextsObjects();
        TranslateAllTexts();
    }
}