﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System.Linq;

public class ULP_HighScores
{
	/// <summary>
	/// HighScore class. Saves a record.
	/// </summary>
	[System.Serializable]
	public class HighScore
	{
		const int N_NAME_CHARACTERS = 20;

		public string playerName;
		public int playerScore;
		public string filepath;

		public HighScore()
		{
		}

		public HighScore(string n, int s)
		{
			SetPlayerName(n);
			SetPlayerScore(s);
		}

		public HighScore(int s)
		{
			SetPlayerName("anonymous");
			SetPlayerScore(s);
		}

		public HighScore(HighScore high)
		{
			SetPlayerName(high.GetPlayerName());
			SetPlayerScore(high.GetPlayerScore());
			SetFilePath(high.GetFilePath());
		}

		public string GetFilePath()
		{
			return filepath;
		}

		public string GetPlayerName()
		{
			return playerName;
		}

		public int GetPlayerScore()
		{
			return playerScore;
		}

		public void SetFilePath(string fp)
		{
			filepath = fp;
		}

		public void SetPlayerName(string n)
		{
			// Limited to N_NAME_CHARACTERS chars
			playerName = n.Trim();
			if (playerName.Length > 20)
			{
				playerName = playerName.Substring(0, N_NAME_CHARACTERS);
			}
		}

		public void SetPlayerScore(int s)
		{
			playerScore = s;
		}
	}

	public class UI_HighScore
	{
		private Text nameLabel;
		private Text scoreLabel;

		public UI_HighScore()
		{
		}

		public Text GetNameLabelTextComponent()
		{
			return nameLabel;
		}

		public Text GetScoreLabelTextComponent()
		{
			return scoreLabel;
		}

		public void SetNameLabelTextComponent(Text text)
		{
			nameLabel = text;
		}

		public void SetScoreLabelTextComponent(Text text)
		{
			scoreLabel = text;
		}
	}

	const string HIGH_SCORES_FOLDER_NAME = "HighScores";

	const string UI_SCORE_NAME = "Score";
	const string PLAYERNAME_LABELNAME = "PlayerName";
	const string PLAYERSCORE_LABELNAME = "PlayerScore";

	public HighScore[] scores, defaultScores;

	public UI_HighScore[] uiScores;

	const string HIGH_SCORES_PREFIX = "highscore";
	const string HIGH_SCORES_NAME_SUFIX = "name";
	const string HIGH_SCORES_SCORE_SUFIX = "score";

	public ULP_HighScores()
	{
		ReloadHighScores(false);
	}

	private void AddDefaultHighScores()
    {
        defaultScores = new HighScore[10];

        defaultScores[0].SetPlayerName("Jaume Castells");
        defaultScores[0].SetPlayerScore(170);

        defaultScores[1].SetPlayerName("Linus Torvalds");
        defaultScores[1].SetPlayerScore(170);

        defaultScores[2].SetPlayerName("Richard Stallman");
        defaultScores[2].SetPlayerScore(170);

        defaultScores[3].SetPlayerName("Guido Van Rossum");
        defaultScores[3].SetPlayerScore(170);

        defaultScores[4].SetPlayerName("Mark Shuttleworth");
        defaultScores[4].SetPlayerScore(170);

        defaultScores[5].SetPlayerName("Brian Behlendorf");
        defaultScores[5].SetPlayerScore(170);

        defaultScores[6].SetPlayerName("Kim Dotcom");
        defaultScores[6].SetPlayerScore(170);

        defaultScores[7].SetPlayerName("Jeff Bezos");
        defaultScores[7].SetPlayerScore(170);

        defaultScores[8].SetPlayerName("Steve Jobs");
        defaultScores[8].SetPlayerScore(170);

        defaultScores[9].SetPlayerName("Bill Gates");
        defaultScores[9].SetPlayerScore(170);
    }

	public void AddNewRecord(HighScore highScore)
	{
		SaveRecord(highScore);
	}

	public void FillUIScores()
	{
		for (int uiIndex = 0; uiIndex < 10; uiIndex++)
		{
			//Debug.Log(scores[uiIndex].GetPlayerName());
			uiScores[uiIndex].GetNameLabelTextComponent().text = scores[uiIndex].GetPlayerName();
			uiScores[uiIndex].GetScoreLabelTextComponent().text = scores[uiIndex].GetPlayerScore().ToString();
		}
	}

	public void GetUIScores()
	{
		uiScores = new UI_HighScore[10];

		for (int uiIndex = 0; uiIndex < 10; uiIndex++)
		{
			uiScores[uiIndex] = new UI_HighScore();
			string uiScoreName = UI_SCORE_NAME + uiIndex.ToString();
			GameObject uiScore = GameObject.Find(uiScoreName);
			uiScores[uiIndex].SetNameLabelTextComponent(uiScore.transform.Find(PLAYERNAME_LABELNAME).GetComponent<Text>());
			uiScores[uiIndex].SetScoreLabelTextComponent(uiScore.transform.Find(PLAYERSCORE_LABELNAME).GetComponent<Text>());
		}
	}

	public bool HighScoreAcquired(int score)
	{
		return (score > scores[scores.Length - 1].GetPlayerScore());
	}

	private void LoadHighScores()
	{
		scores = new HighScore[10];

		if (PlayerPrefs.HasKey(GetPlayerPrefStringName(0)))
		{
			LoadPlayerPrefsToScores();
		}
		else
		{
			ResetHighScores();
		}
	}

	public void ReloadHighScores(bool withUI)
	{
		LoadHighScores();
		SortRecordsByScores();
		if (withUI)
		{
			ReloadHighScoresUI();
		}
	}

	private void ReloadHighScoresUI()
	{
		if (scores[0] != null)
		{
			GetUIScores();
			FillUIScores();
		}
	}

	private void SortRecordsByScores()
	{
		if (scores[0] != null)
		{
			System.Array.Sort(scores, delegate (HighScore sc1, HighScore sc2)
			{
				return sc1.GetPlayerScore().CompareTo(sc2.GetPlayerScore());
			});

			System.Array.Reverse(scores);
		}
	}

	private string GetPlayerPrefStringName(int position)
	{
		return (HIGH_SCORES_PREFIX + position.ToString() + HIGH_SCORES_NAME_SUFIX);
	}

	private string GetPlayerPrefStringScore(int position)
    {
		return (HIGH_SCORES_PREFIX + position.ToString() + HIGH_SCORES_SCORE_SUFIX);
    }

	private void LoadPlayerPrefToScore(int position)
	{
		scores[position].SetPlayerName(PlayerPrefs.GetString(GetPlayerPrefStringName(position)));
		scores[position].SetPlayerScore(PlayerPrefs.GetInt(GetPlayerPrefStringScore(position)));
	}

	public void LoadPlayerPrefsToScores()
	{
		for (int num = 0; num < scores.Length; num++)
		{
			LoadPlayerPrefToScore(num);
		}
	}

	private void LoadScoreToPlayerPref(int position)
	{
		PlayerPrefs.SetString(GetPlayerPrefStringName(position), scores[position].GetPlayerName());
		PlayerPrefs.SetInt(GetPlayerPrefStringScore(position), scores[position].GetPlayerScore());
	}

	public void LoadScoresToPlayerPrefs()
	{
		for (int num = 0; num < scores.Length; num++)
		{
			LoadScoreToPlayerPref(num);
		}
	}

	private void ResetHighScore(int position)
    {
        if (position < scores.Length)
        {
            scores[position].SetPlayerName(defaultScores[position].GetPlayerName());
            scores[position].SetPlayerScore(defaultScores[position].GetPlayerScore());
        }
        else
        {
            Debug.LogError("Score position out of table!");
        }
    }

	public void ResetHighScores()
    {
        AddDefaultHighScores();

		for (int a = 0; a < scores.Length; a++)
		{
			ResetHighScore(a);
		}
    }
    
	public void SavePlayerPrefsRecords()
	{
		// First load scores to playerprefs
		LoadScoresToPlayerPrefs();
		//// This save all prefs, highscores included.
		PlayerPrefs.Save();
	}

	public void SaveRecord(HighScore highScore)
	{
		//int currentPos = scores.Length - 1;
		//bool recordSaved = false;

		//while ((currentPos > 0) && (!recordSaved))
		//{
		//	if (highScore.GetPlayerScore() > scores[currentPos].GetPlayerScore())
		//	{
		//		if (currentPos < (scores.Length - 1))
		//		{
		//			scores[currentPos].SetPlayerName(scores[cu]);
		//		}
		//		currentPos--;
		//	}
		//}
		//scores[currentPos].SetPlayerName(highScore.GetPlayerName());
		//scores[currentPos].SetPlayerScore(highScore.GetPlayerScore());
		scores[scores.Length - 1].SetPlayerName(highScore.GetPlayerName());
		scores[scores.Length - 1].SetPlayerScore(highScore.GetPlayerScore());
		SortRecordsByScores();
		SavePlayerPrefsRecords();
	}

}
