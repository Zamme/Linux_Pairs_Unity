﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ULP_Card : MonoBehaviour 
{
    public int cardValue;

    private ULP_GameManager gameManager;

    private bool isShowing = false;

    private ParticleSystem ps;

	void Start () 
    {
        gameManager = GameObject.Find("GameManager").GetComponent<ULP_GameManager>();
        if (gameManager)
        {
            SetReverseImage();
            ps = GetParticleSystem();
        }
	}

    public void EmitSuccesEffects()
    {
        ps.Emit(10);
    }

    private ParticleSystem GetParticleSystem()
    {
        return gameObject.GetComponent<ParticleSystem>();
    }

    public void Pressed()
    {
        if (isShowing)
        {
            SetReverseImage();
        }
        else
        {
            SetCardImage();
        }

        isShowing = !isShowing;

        gameManager.CardPressed(gameObject);
    }

    public void ResetCard()
    {
        isShowing = false;
        SetReverseImage();
    }

    public void SetCardValue(int val)
    {
        cardValue = val;
    }

    void SetCardImage()
    {
        GetComponent<Image>().sprite = gameManager.cardsImages[cardValue];
    }

    void SetReverseImage()
    {
        GetComponent<Image>().sprite = gameManager.reverseImage;
    }
}
