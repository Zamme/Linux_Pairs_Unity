﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ULP_SoundManager : MonoBehaviour
{
    public AudioClip gameMusic;
    public AudioClip cardClick;
    public AudioClip cardReveal;
    public AudioClip fail00;
    public AudioClip fail01;
    public AudioClip succes00;
    public AudioClip succes01;

    public const int NUM_AUDIO_SOURCES = 10;
    private AudioSource[] audioSources;
    private AudioSource availableSource;

    public enum AudioSample { GameMusic, CardClick, CardReveal, Fail00, Fail01, Succes00, Succes01}

    void Awake()
    {
        GetAudioSources();
    }

    private void Start()
    {
    }

    private AudioSource FindFreeSource()
    {
        AudioSource audioSource = null;

        int iAS = 0;
        while (!audioSource && (iAS < audioSources.Length))
        {
            if (!audioSources[iAS].isPlaying)
            {
                audioSource = audioSources[iAS];
            }
            else
            {
                iAS++;
            }
        }

        return audioSource;
    }

    private void GetAudioSources()
    {
        audioSources = new AudioSource[NUM_AUDIO_SOURCES];

        for (int ias = 0; ias < NUM_AUDIO_SOURCES; ias++)
        {
            audioSources[ias] = gameObject.AddComponent<AudioSource>();    
        }

        //Debug.Log(audioSources.Length + " audio sources created.");
    }

    private bool GetFreeSource()
    {
        availableSource = FindFreeSource();

        return (availableSource != null);
    }

    private bool IsMusicPlaying()
    {
        bool playing = false;

        foreach (AudioSource audioSource in audioSources)
        {
            if (audioSource.clip == gameMusic)
            {
                playing = true;
            }
        }

        return playing;
    }

    public void PlayAudio(AudioSample audioSample)
    {
        if (GetFreeSource())
        {
            switch (audioSample)
            {
                case AudioSample.GameMusic:
                    if (!IsMusicPlaying())
                    {
                        availableSource.clip = gameMusic;
                        availableSource.loop = true;
                    }
                    break;
                case AudioSample.CardClick:
                    availableSource.clip = cardClick;
                    availableSource.loop = false;
                    break;
                case AudioSample.CardReveal:
                    availableSource.clip = cardReveal;
                    availableSource.loop = false;
                    break;
                case AudioSample.Fail00:
                    availableSource.clip = fail00;
                    availableSource.loop = false;
                    break;
                case AudioSample.Fail01:
                    availableSource.clip = fail01;
                    availableSource.loop = false;
                    break;
                case AudioSample.Succes00:
                    availableSource.clip = succes00;
                    availableSource.loop = false;
                    break;
                case AudioSample.Succes01:
                    availableSource.clip = succes01;
                    availableSource.loop = false;
                    break;
            }

            availableSource.Play();
        }
        else
        {
            Debug.LogError("No audio source available!");
        }
    }

}
